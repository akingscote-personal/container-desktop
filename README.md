# container-desktop
Build the container, then just run with:
```
docker run -it --rm --publish="3389:3389/tcp" -e USER=ashley -e PASSWORD=ashley desktop:dev /bin/bash
```

Container size is around 721MB